package stats

import (
	"net/http"
)

type serveAsset struct {
	data []byte
	mime string
}

func NewAssetHandler(mime string, data []byte) http.Handler {
	sa := new(serveAsset)
	sa.mime = mime
	sa.data = data
	return sa
}

func (sa *serveAsset) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Content-Type", sa.mime)
	resp.Write(sa.data)
}
