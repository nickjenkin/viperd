package stats

import (
	"html/template"
	"net/http"
	"viperd/stats/assets"
	"viperd/stats/assets/templates"
)

type StatsHandler struct {
	templates *template.Template
	mux       *http.ServeMux
}

type statData struct {
	Name string
}

func NewStatsHandler() http.Handler {
	sh := new(StatsHandler)

	sh.templates, _ = template.New("base").Parse(string(templates.TemplateBase_html()))
	sh.templates.Parse(string(templates.TemplateIndex_html()))

	sh.mux = http.NewServeMux()
	sh.mux.Handle("/__viperstats/logo.png", NewAssetHandler("image/png", assets.Logo_png()))
	sh.mux.Handle("/__viperstats/stats.css", NewAssetHandler("text/css", assets.Stats_css()))
	sh.mux.Handle("/__viperstats/stats.js", NewAssetHandler("application/javascript", assets.Stats_js()))
	sh.mux.Handle("/__viperstats/", sh)

	return sh.mux
}

func (sh *StatsHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	data := &statData{"nick"}
	sh.templates.ExecuteTemplate(resp, "index", data)
	assets.Logo_png()
}
