package assets

import (
	"reflect"
	"unsafe"
)

var _Stats_js = ""

// Stats_js returns raw file data.
//
// WARNING: The returned byte slice is READ-ONLY.
// Attempting to alter the slice contents will yield a runtime panic.
func Stats_js() []byte {
	var empty [0]byte
	sx := (*reflect.StringHeader)(unsafe.Pointer(&_Stats_js))
	b := empty[:]
	bx := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	bx.Data = sx.Data
	bx.Len = len(_Stats_js)
	bx.Cap = bx.Len
	return b
}
