#!/bin/bash
function test {
    "$@"
    status=$?
    if [ $status -ne 0 ]; then
        exit
    fi
    return $status
}

if [ ! -f "js/yuicompressor-2.4.7.jar" ]; then
	echo "downloading yuicompressor"
	test curl http://repo1.maven.org/maven2/com/yahoo/platform/yui/yuicompressor/2.4.7/yuicompressor-2.4.7.jar -o js/yuicompressor-2.4.7.jar
fi

# compress js file
#test java -jar js/yuicompressor-2.4.7.jar --type js -o server/assets/viper-min.js server/assets/viper.js
cp server/assets/viper.js server/assets/viper-min.js

# build viper js file
test go-bindata -p assets -o server/assets/viper.js.go -i server/assets/viper-min.js  -u -m -f Viper_js

# other assets
test go-bindata -p assets -o server/assets/http502.html.go -i server/assets/502.html  -u -m -f HttpError_502

# stats assets
test go-bindata -p templates -o stats/assets/templates/base.html.go -i stats/assets/templates/base.html  -u -m -f TemplateBase_html
test go-bindata -p templates -o stats/assets/templates/index.html.go -i stats/assets/templates/index.html  -u -m -f TemplateIndex_html
test go-bindata -p assets -o stats/assets/viperd.png.go -i stats/assets/viperd.png  -u -m -f Logo_png
test go-bindata -p assets -o stats/assets/stats.css.go -i stats/assets/stats.css  -u -m -f Stats_css
test go-bindata -p assets -o stats/assets/stats.js.go -i stats/assets/stats.js  -u -m -f Stats_js



# install
test go install viperd

# run
test viperd -reverse direct.nickjenkin.com:80 -stats
