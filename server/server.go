/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package server

import (
	"net/http"
	"viperd/diffindex"
	"viperd/stats"
)

type ViperServerConfiguration struct {
	BindAddress         string
	ReverseProxyAddress string
	StatsEnabled        bool
}

type ViperServer struct {
	config *ViperServerConfiguration
}

func NewViperServer(config *ViperServerConfiguration) *ViperServer {
	vs := new(ViperServer)
	vs.config = config
	return vs
}

func (vs *ViperServer) Start() {

	diffIndex := diffindex.New()
	go warmIndex(diffIndex)
	ah := newAssetHandler()
	vh := newViperHandler(vs.config.ReverseProxyAddress, diffIndex)
	hh := newHashHandler(diffIndex.GetDiffStore())

	http.Handle("/__viper.js", ah)
	http.Handle("/__viperh", hh)

	if vs.config.StatsEnabled {
		sh := stats.NewStatsHandler()
		http.Handle("/__viperstats/", sh)
	}

	http.Handle("/", vh)

	err := http.ListenAndServe(vs.config.BindAddress, nil)

	if err != nil {
		panic(err.Error())
	}
}

func warmIndex(di *diffindex.DiffIndex) {
	//di.WarmIndex("http://direct.nickjenkin.com/blog/", "viper.nickjenkin.com:8080")
	di.WarmIndex("http://direct.nickjenkin.com/blog/2012/12/sometimes-you-need-a-different-challenge/", "viper.nickjenkin.com:8080")
	di.WarmIndex("http://direct.nickjenkin.com/blog/2011/03/transition-into-losing-your-database/", "viper.nickjenkin.com:8080")
	di.WarmIndex("http://direct.nickjenkin.com/blog/2011/03/transition-into-losing-your-database/", "viper.nickjenkin.com:8080")
	di.WarmIndex("http://direct.nickjenkin.com/blog/2009/04/naive-bayes-in-hadoop/", "viper.nickjenkin.com:8080")
	di.Rebuild()
}
