package server

import (
	"net/http"
	"testing"
)

func Test_supportsGzip_1(t *testing.T) {
	req := mockHTTPRequest()
	req.Header.Add("Accept-Encoding", "rot13")

	if supportsGzip(req) {
		t.Error("expected false")
	} else {
		t.Log("test passed")
	}
}

func Test_supportsGzip_2(t *testing.T) {
	req := mockHTTPRequest()
	req.Header.Add("Accept-Encoding", "gzip")

	if !supportsGzip(req) {
		t.Error("expected true")
	} else {
		t.Log("test passed")
	}
}

func Test_supportsGzip_3(t *testing.T) {
	req := mockHTTPRequest()
	req.Header.Add("Accept-Encoding", "foo,gzip,bar,baz,rot13")

	if !supportsGzip(req) {
		t.Error("expected true")
	} else {
		t.Log("test passed")
	}
}

func Test_supportsGzip_4(t *testing.T) {
	req := mockHTTPRequest()
	req.Header.Add("Accept-Encoding", "foo,bar,baz,rot13")

	if supportsGzip(req) {
		t.Error("expected false")
	} else {
		t.Log("test passed")
	}
}

func mockHTTPRequest() *http.Request {

	req, _ := http.NewRequest("GET", "http://example.com", nil)
	return req
}
