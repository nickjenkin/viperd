/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package server

import (
	"bytes"
	"compress/gzip"

	"net/http"
	"time"
	"viperd/server/assets"
)

type assetHandler struct {
	viperJsGzip []byte
}

func newAssetHandler() *assetHandler {
	ah := new(assetHandler)
	buf := new(bytes.Buffer)

	gz := gzip.NewWriter(buf)
	gz.Write(assets.Viper_js())
	gz.Close()

	ah.viperJsGzip = buf.Bytes()

	return ah
}
func (ah *assetHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	defer logRequest(req)
	resp.Header().Add("X-Viper", "pew pew pew X-Wing")
	resp.Header().Add("Content-Type", "application/javascript")
	resp.Header().Add("Cache-Control", "max-age=31536000")

	expires := time.Now().UTC().AddDate(1, 0, 0)
	resp.Header().Add("Expires", expires.Format(time.RFC1123))
	if supportsGzip(req) {
		resp.Header().Set("Content-Encoding", "gzip")
		resp.Write(ah.viperJsGzip)
	} else {
		resp.Write(assets.Viper_js())
	}

}
