package assets

import (
	"crypto/md5"
	"fmt"
)

func Viper_js_md5() string {
	h := md5.New()
	h.Write(Viper_js())
	return fmt.Sprintf("%x", h.Sum(nil))
}
