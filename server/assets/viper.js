/**
__
| \
=[_|H)--._____
=[+--,-------' - -- - --  - - - - pew pew pew
 [|_/""


Format:
[
	["132sdf654sd65f4s6d5f4sf","hello wolld"],
	["sd2f1sd3f21sd3f21sd3f1sd32f"],
	["sdfsdfsdfsd3f21s3df2"]
]



*/
(function() {

	var geid = function(e){if(typeof e=='string')e=document.getElementById(e);return e};
	var collect = function(a,f){var n=[];for(var i=0;i<a.length;i++){var v=f(a[i]);if(v!=null)n.push(v)}return n};

	var ajax={};
	ajax.x=function(){try{return new ActiveXObject('Msxml2.XMLHTTP')}catch(e){try{return new ActiveXObject('Microsoft.XMLHTTP')}catch(e){return new XMLHttpRequest()}}};
	ajax.serialize=function(f){var g=function(n){return f.getElementsByTagName(n)};var nv=function(e){if(e.name)return encodeURIComponent(e.name)+'='+encodeURIComponent(e.value);else return ''};var i=collect(g('input'),function(i){if((i.type!='radio'&&i.type!='checkbox')||i.checked)return nv(i)});var s=collect(g('select'),nv);var t=collect(g('textarea'),nv);return i.concat(s).concat(t).join('&');};
	ajax.send=function(u,f,m,a){var x=ajax.x();x.open(m,u,true);x.onreadystatechange=function(){if(x.readyState==4)f(x.responseText)};if(m=='POST')x.setRequestHeader('Content-type','application/x-www-form-urlencoded');x.send(a)};
	ajax.post=function(url,func,args){ajax.send(url,func,'POST',args)};
	ajax.submit=function(url,elm,frm){var e=geid(elm);var f=function(r){e.innerHTML=r};ajax.post(url,f,ajax.serialize(frm))};
	_viper_w = function(state) {
		document.open();
		for(var i=0; i < state.length; i++) {
			document.write(state[i]);
		}
		document.close();
	}
	_viper = function(page) {
		__viper_state = [];
		var hashlist = [];
		var keylist = {};
		for(var i=0; i < page.length; i++) {

			if("h" in page[i]) {
				hash = page[i].h;
				if(typeof localStorage["_viper."+hash] != "undefined") {
					__viper_state[i] = localStorage["_viper."+hash];
					console.log("loading "+ hash + " from localStorage");
				} else {
					hashlist.push(hash);
					if(!(hash in keylist)) {
						keylist[hash] = [];
					}
					keylist[hash].push(i);
				}
			}
			else {
				__viper_state[i] = page[i].d;
			}
		}


		var hashstr = hashlist.join(",");
		if(hashlist.length > 0) {
			console.log("doing request", hashlist.length);
			ajax.post("/__viperh", function(data) {
				resp = JSON.parse(data);
				console.log(resp);

				if(typeof resp.Hash != "undefined") {


					var count = 0;
					for(var key in resp.Hash) {
						for(var i=0; i < keylist[key].length; i++) {
							__viper_state[keylist[key][i]] = resp.Hash[key];
						}

						localStorage["_viper."+key] = resp.Hash[key];
						count++;
					}
					console.log("got", count, "hashes back");
				}

				_viper_w(__viper_state);

				delete __viper_state;
			}, "h=" + hashstr);
		}
		else {
				_viper_w(__viper_state);
		}
	};


})();