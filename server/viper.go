/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package server

import (
	"compress/gzip"

	"encoding/json"

	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"viperd/diffindex"
	"viperd/server/assets"
)

type viperHandler struct {
	jsMd5 string

	proxy     *http.Transport
	proxyHost string
	proxyPort string

	diffIndex *diffindex.DiffIndex
}

func newViperTransport(proxyAddress string) *http.Transport {
	return &http.Transport{
		DisableKeepAlives:  false,
		DisableCompression: false,
		Dial: func(proto, addr string) (c net.Conn, err error) {
			return net.Dial(proto, proxyAddress)
		},
		Proxy: http.ProxyFromEnvironment,
	}
}

func newViperHandler(proxyAddress string, diffIndex *diffindex.DiffIndex) *viperHandler {
	vh := new(viperHandler)
	vh.jsMd5 = assets.Viper_js_md5()
	vh.proxy = newViperTransport(proxyAddress)
	hostIp, hostPort, _ := net.SplitHostPort(proxyAddress)
	vh.proxyHost = hostIp
	vh.proxyPort = hostPort
	vh.diffIndex = diffIndex
	return vh
}

func (vh *viperHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	defer logRequest(req)

	proxyResp, err := vh.forwardRequest(req)

	if err != nil {
		log.Println("Error connecting to backend", err)
		writeError(resp)
		return
	}

	// copy headers from backend to frontend
	copyHeader(resp.Header(), proxyResp.Header)

	/*if err != nil {
		log.Println("READ ERROR", err)
		writeError(resp)
		return
	}*/

	//log.Println("RESPONSE HEADER")
	//dumpHeader(proxyResp.Header)

	if isRedirect(proxyResp) {
		vh.forwardRedirect(proxyResp, resp, req)
		return
	} else if isHTML(proxyResp) && !isXHR(req) {
		vh.optimizeRequest(proxyResp, resp, req)
		return
	} else {
		// pass thru
		//data, err := ioutil.ReadAll(proxyResp.Body)
		resp.WriteHeader(proxyResp.StatusCode)
		io.Copy(resp, proxyResp.Body)
		//resp.Write(data)
		return
	}
}

func (vh *viperHandler) forwardRequest(req *http.Request) (*http.Response, error) {
	req = vh.prepareProxyRequest(req)

	resp, err := vh.proxy.RoundTrip(req)
	return resp, err
}

func (vh *viperHandler) forwardRedirect(proxyResp *http.Response, resp http.ResponseWriter, req *http.Request) {
	loc := proxyResp.Header.Get("Location")

	rdirUrl, _ := url.Parse(loc)
	rdirUrl.Scheme = "http"

	rdirUrl.Host = req.Host

	http.Redirect(resp, req, rdirUrl.String(), proxyResp.StatusCode)
	log.Println("redirect to", resp.Header().Get("Location"), loc)
}

// mostly copied from httputil.ReverseProxy
func (vh *viperHandler) prepareProxyRequest(req *http.Request) *http.Request {

	outreq := new(http.Request)
	*outreq = *req // includes shallow copies of maps, but okay

	outreq.Proto = "HTTP/1.1"
	outreq.ProtoMajor = 1
	outreq.ProtoMinor = 1
	outreq.Close = false
	outreq.Header.Del("If-Modified-Since")
	outreq.Header.Del("If-None-Match")

	outreq.URL.Scheme = "http"

	// Remove the connection header to the backend.  We want a
	// persistent connection, regardless of what the client sent
	// to us.  This is modifying the same underlying map from req
	// (shallow copied above) so we only copy it if necessary.
	if outreq.Header.Get("Connection") != "" {
		outreq.Header = make(http.Header)
		copyHeader(outreq.Header, req.Header)
		outreq.Header.Del("Connection")
	}

	if clientIp, _, err := net.SplitHostPort(req.RemoteAddr); err == nil {
		outreq.Header.Set("X-Forwarded-For", clientIp)
	}

	return outreq
}

func (vh *viperHandler) optimizeRequest(proxyResp *http.Response, resp http.ResponseWriter, req *http.Request) {
	resp.Header().Del("Content-Encoding")
	resp.Header().Del("Content-Length")

	req.URL.Host = req.Host

	out := getGzipWriter(resp, req)
	defer out.Close()

	// write out the JS file now so the client can load it.
	out.Write(vh.viperJsInclude())
	var data []byte
	if strings.Contains(proxyResp.Header.Get("Content-Encoding"), "gzip") {
		gz, _ := gzip.NewReader(proxyResp.Body)

		data, _ = ioutil.ReadAll(gz)
		gz.Close()

	} else {
		data, _ = ioutil.ReadAll(proxyResp.Body)
	}

	blocks := vh.diffIndex.ProcessPage(req.URL, data)

	log.Println("Blocks Found", len(blocks.PageBlocks))

	out.Write([]byte("<script>_viper("))
	for _, b := range blocks.PageBlocks {
		if b.Hash != "" {
			b.Data = ""
		}
	}

	jsx, _ := json.Marshal(blocks.PageBlocks)
	out.Write(jsx)

	out.Write([]byte(")</script>"))

}

// viperJsInclude returns the base HTML script include for the viper.js script
func (vh *viperHandler) viperJsInclude() []byte {
	inc := fmt.Sprintf("<script src=\"/__viper.js?=%s\"></script>", vh.jsMd5[0:6])
	return []byte(inc)
}

func (vh *viperHandler) viperJsFooter() []byte {
	foot := ");</script><noscript><img src='/__viper_nojs'><meta http-equiv=\"refresh\" content=\"1\"></noscript>"

	return []byte(foot)
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func isRedirect(resp *http.Response) bool {
	return resp.Header.Get("Location") != "" && resp.StatusCode >= 300 && resp.StatusCode < 400
}
func isHTML(resp *http.Response) bool {
	return strings.HasPrefix(resp.Header.Get("Content-Type"), "text/html") && resp.StatusCode == 200
}

func isXHR(req *http.Request) bool {
	return strings.Contains(req.Header.Get("X-Requested-With"), "XMLHttpRequest")
}

func dumpHeader(src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			log.Printf("%s: %s", k, v)
		}
	}
}

func writeGzipData(data []byte, resp http.ResponseWriter, req *http.Request) {
	if supportsGzip(req) {
		resp.Header().Add("Content-Encoding", "gzip")
		gz := gzip.NewWriter(resp)
		gz.Write(data)
		gz.Close()
	} else {
		resp.Header().Del("Content-Encoding")
		resp.Write(data)
	}
}

func getGzipWriter(resp http.ResponseWriter, req *http.Request) io.WriteCloser {
	if supportsGzip(req) {
		resp.Header().Add("Content-Encoding", "gzip")
		gz := gzip.NewWriter(resp)
		return gz
	}
	return &fakeCloser{resp}
}

func writeError(resp http.ResponseWriter) {
	resp.Header().Set("Content-Type", "text/html")
	resp.WriteHeader(http.StatusBadGateway)
	resp.Write(assets.HttpError_502())
}

type fakeCloser struct {
	io.Writer
}

func (fc *fakeCloser) Close() error {
	return nil
}
