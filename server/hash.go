/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"viperd/pagestore"
)

type hashResponse struct {
	Hash map[string]*string
}

func newHashResponse() *hashResponse {
	hr := new(hashResponse)
	hr.Hash = make(map[string]*string)
	return hr
}

type hashHandler struct {
	body      string
	pageStore *pagestore.PageStore
}

func newHashHandler(pageStore *pagestore.PageStore) *hashHandler {
	vh := new(hashHandler)
	vh.pageStore = pageStore
	go vh.fetch_test()

	return vh
}

func (vh *hashHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	go logRequest(req)
	hashlist := strings.Split(req.FormValue("h"), ",")

	result := newHashResponse()

	for _, hash := range hashlist {
		if res, ok := vh.pageStore.Get(hash); ok {
			result.Hash[hash] = res
		}
	}
	b, _ := json.Marshal(result)
	fmt.Println(string(b))
	resp.Write(b)
}

func (hh *hashHandler) fetch_test() {
	resp, _ := http.Get("http://nickjenkin.com")
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	hh.body = string(body)
}
