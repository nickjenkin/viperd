/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
	"fmt"
	"viperd/server"
)

func parseFlags() *server.ViperServerConfiguration {
	config := new(server.ViperServerConfiguration)

	flag.StringVar(&config.BindAddress, "bind", ":8080", "The address to bind to in the format ip:port (e.g. 0.0.0.0:8080)")
	flag.StringVar(&config.ReverseProxyAddress, "reverse", "127.0.0.1:80", "Address to forward all requests to in the format: 127.0.0.1:80")
	flag.BoolVar(&config.StatsEnabled, "stats", false, "Enable the stats page. INSECURE!")

	flag.Parse()

	return config
}

func main() {

	config := parseFlags()
	fmt.Printf("viperd (%s -> %s)\n", config.BindAddress, config.ReverseProxyAddress)
	vs := server.NewViperServer(config)
	vs.Start()
}
