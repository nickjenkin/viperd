package pagestore

import (
	"testing"
)

func Test_PageStore_Store(t *testing.T) {

	ps := New()

	hash := ps.Store("hello world")

	if hash == "" {
		t.Error("hash should not be empty string")
	}
	v, _ := ps.Get(hash)
	if *v != "hello world" {
		t.Error("recieved wrong page value")
	}

}

func Test_PageStore_Get(t *testing.T) {

	ps := New()
	v, ok := ps.Get("hash that does not exist")

	if v != nil || ok {
		t.Error("expected nil response for hash that does not exist")
	}
}
