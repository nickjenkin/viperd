/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pagestore

import (
	"sync"
)

type PageStore struct {
	pages map[string]*pageValue

	storeLock *sync.Mutex
}

func New() *PageStore {
	hs := new(PageStore)
	hs.pages = make(map[string]*pageValue)
	hs.storeLock = new(sync.Mutex)
	return hs
}

// Store takes a page of data, stores it in the hash table and
// returns it's hash (sha1)
func (hs *PageStore) Store(data string) string {
	hs.storeLock.Lock()
	defer hs.storeLock.Unlock()

	page := newPageValue(&data)
	hash := page.Hash()
	hs.pages[hash] = page
	return hash
}

// Get takes a hash and returns it's corresponding page
// will return nil if hash does not exist
func (hs *PageStore) Get(hash string) (*string, bool) {
	if page, ok := hs.pages[hash]; ok {
		return page.data, ok
	}
	return nil, false
}
