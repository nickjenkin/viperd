package pagestore

import (
	"testing"
)

func Test_newPageValue(t *testing.T) {
	str := "foo"
	page := newPageValue(&str)

	if str != *page.data {
		t.Error("pageValue.data is not being set on initalization")
	} else {
		t.Log("test passed")
	}
}

func Test_pageValue_Hash(t *testing.T) {

	hashCheck(t, "hello", "aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d")
	hashCheck(t, "world", "7c211433f02071597741e6ff5a8ea34789abbf43")

}

func hashCheck(t *testing.T, page string, expected string) {
	p := newPageValue(&page)

	if p.Hash() != expected {
		t.Errorf("hash error, expected %s got %s", expected, p.Hash())
	} else {
		t.Log("test passed")
	}
}
