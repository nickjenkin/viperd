package diff

import (
	"testing"
)

func Test_Diff_Common_1(t *testing.T) {

	com := getCommon("AAAAAAA", "BBBBBBB", 1)

	if len(com) > 0 {
		t.Error("expected no diffs")
	}

}

func Test_Diff_Common_2(t *testing.T) {

	com := getCommon("AAAAABBBB", "AAACCCC", 1)

	if len(com) != 1 {
		t.Error("expected one diff", com)
		return
	}

	if com[0] != "AAA" {
		t.Error("expected diff to be `AAA>`", com)
	}

}

func Test_Diff_Common_3(t *testing.T) {

	com := getCommon("BBBBAAAAA", "CCCCAAA", 1)

	if len(com) != 1 {
		t.Error("expected one diff", com)
		return
	}

	if com[0] != "AAA" {
		t.Error("expected diff to be `AAA`", com)
	}

}

func Test_Diff_Common_4(t *testing.T) {

	com := getCommon("111 cats 222", "333 cats 444", 1)

	if len(com) != 1 {
		t.Error("expected one diff", com)
	}

	if com[0] != " cats " {
		t.Error("expected diff to be `<space>cats<space>`", com)
	}

}

func Test_Diff_Common_5(t *testing.T) {

	diffTest(t, "hello", "world", []string{"l"})
	diffTest(t, "aaaaa", "zzzzz", []string{})
	diffTest(t, "hello", "hello", []string{"hello"})
	diffTest(t, "hello foo cat", "hello bar cat", []string{"hello ", " cat"})
	diffTest(t, "cat says hello", "dog says hello", []string{" says hello"})
	diffTest(t, "cat says hello", "", []string{})
	diffTest(t, "", "212131", []string{})

}

func diffTest(t *testing.T, text1, text2 string, expected []string) {

	com := getCommon(text1, text2, 1)

	if !compareResults(com, expected) {
		t.Error("expected", expected, "recieved", com)
	}
}

func compareResults(result, expected []string) bool {

	if len(result) != len(expected) {
		return false
	}

	for i, r := range result {
		if r != expected[i] {
			return false
		}
	}

	return true

}

func getCommon(text1, text2 string, minmatch int) []string {
	df := New(text1, text2)
	df.MinimumMatch = minmatch
	return df.Common()
}
