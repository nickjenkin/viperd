/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package diff

import (
	"index/suffixarray"
)

type Diff struct {
	text1        string
	text2        string
	t1index      *suffixarray.Index
	t2index      *suffixarray.Index
	MinimumMatch int
}

func New(text1 string, text2 string) *Diff {
	diff := new(Diff)
	diff.text1 = text1
	diff.text2 = text2
	diff.MinimumMatch = 100
	return diff
}
func (diff *Diff) Common() []string {

	result := make([]string, 0)

	// are the strings the same?
	if diff.text1 == diff.text2 {
		result = append(result, diff.text1)
		return result
	}

	// no empty strings
	if len(diff.text1) == 0 || len(diff.text2) == 0 {
		return result
	}

	head := diff.headSearch()

	if len(head) > diff.MinimumMatch {
		result = append(result, head)
		diff.text1 = diff.text1[len(head):]
		diff.text2 = diff.text2[len(head):]
	}

	tail := diff.tailSearch()

	if len(tail) > diff.MinimumMatch {
		result = append(result, tail)
		diff.text1 = diff.text1[0 : len(diff.text1)-len(tail)]
		diff.text2 = diff.text2[0 : len(diff.text2)-len(tail)]

	}

	mid := diff.middleSearch()
	if len(mid) > 0 {
		for _, s := range mid {
			result = append(result, s)
		}

	}

	return result
}

func (diff *Diff) middleSearch() []string {
	//	t1idx := suffixarray.New([]byte(diff.text1))
	t2idx := suffixarray.New([]byte(diff.text2))
	t1b := []byte(diff.text1)
	pos := 1
	startpos := 0
	slen := len(t1b)
	result := make([]string, 0)
	// TODO: divide and conquer this
	for {

		if pos >= slen {
			break
		}

		res := t2idx.Lookup(t1b[startpos:pos], 1)
		if len(res) == 0 {
			if pos-startpos > diff.MinimumMatch {
				result = append(result, string(t1b[startpos:pos-1]))
			}
			startpos = pos

		}
		pos++
	}

	return result
}

func (diff *Diff) headSearch() string {

	t1 := []rune(diff.text1)
	t2 := []rune(diff.text2)

	for i, c := range t1 {
		if c != t2[i] {
			if i == 0 {
				return ""
			}
			return string(t2[0:i])
		}
	}

	return string(t1)

}

func (diff *Diff) tailSearch() string {
	t1 := []rune(diff.text1)
	t2 := []rune(diff.text2)

	t1pos := len(t1) - 1
	t2pos := len(t2) - 1

	for {
		if t1[t1pos] != t2[t2pos] {
			break
		} else {
			if t1pos == 0 || t2pos == 0 {
				break
			}

			t1pos--
			t2pos--
		}
	}
	if len(t1) != t1pos+1 {
		return string(t1[t1pos+1:])
	}
	return ""
}
