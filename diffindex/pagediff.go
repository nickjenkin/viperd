package diffindex

type pageDiff struct {
	hash  string
	hit   uint64
	miss  uint64
	paths []string
}

func newPageDiff(hash string) *pageDiff {
	pd := new(pageDiff)
	pd.hash = hash
	pd.paths = make([]string, 0)
	return pd
}

func (pd *pageDiff) AddPath(path string) {
	pd.paths = append(pd.paths, path)
}
