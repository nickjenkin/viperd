/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package diffindex

import (
	"crypto/sha1"
	"fmt"
	"index/suffixarray"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sort"
	"viperd/diff"
	"viperd/pagestore"
)

type DiffIndex struct {
	diffStore *pagestore.PageStore
	pages     []*page
	index     *index
}

type PageBlock struct {
	// The Hash value used to store this diff in the DiffStore
	// if the value of the hash is the empty string, this block
	// was not stored in the diff index.
	Hash string `json:"h,omitempty"`

	Data string `json:"d,omitempty"`
}

func newPageBlock(hash, data string) *PageBlock {
	pb := new(PageBlock)
	pb.Hash = hash
	pb.Data = data
	return pb

}

type page struct {
	hash   string // hash of page.data
	data   string
	delete bool
	url    *url.URL
	diffs  []string // hashes of diffs extracted from this page
}
type DiffMachineResult struct {
	// An ordered list of page blocks whereby, concat(PageBlock.Data) = input page
	PageBlocks []*PageBlock
}

func newDiffMachineResult() *DiffMachineResult {
	dmr := new(DiffMachineResult)
	dmr.PageBlocks = make([]*PageBlock, 0)
	return dmr
}
func newPage(url *url.URL, data string) *page {
	p := new(page)
	p.url = url
	p.data = data
	p.hash = hash([]byte(data))
	p.diffs = make([]string, 0)
	p.delete = false

	return p
}

func New() *DiffIndex {
	dm := new(DiffIndex)
	dm.diffStore = pagestore.New()
	dm.pages = make([]*page, 0)
	dm.index = newIndex()
	return dm
}

func (dm *DiffIndex) GetDiffStore() *pagestore.PageStore {
	return dm.diffStore
}

// AddPage adds a page to the diff index, the page is compared
// against existing pages in the index and any significant differences
// are extracted and placed into the index.
func (dm *DiffIndex) AddPage(urlx *url.URL, data string) {
	p := newPage(urlx, data)

	dm.pages = append(dm.pages, p)
}

// Fetches the given URL and adds it to the index
func (dm *DiffIndex) WarmIndex(urlx string, host string) {
	resp, err := http.Get(urlx)

	log.Println("Warming with", urlx)
	if err != nil {
		log.Println("WarmIndex:", err)
		return
	}
	data, _ := ioutil.ReadAll(resp.Body)
	requrl, _ := url.Parse(urlx)

	if host != "" {
		requrl.Host = host
	}
	dm.AddPage(requrl, string(data))
}

// Rebuild reindexes the pages currently in the index.
func (dm *DiffIndex) Rebuild() {

	index := newIndex()

	log.Println("Rebuilding diffindex")

	for i, p := range dm.pages {
		for j, p2 := range dm.pages {
			if i == j || p.hash == p2.hash {
				continue
			}

			diffs := diffPages(p, p2)

			for _, diff := range diffs {
				hash := hash([]byte(diff))
				index.AddDiff(p.url, hash)
				dm.diffStore.Store(diff)

			}
		}
	}

	for _, di := range index.domains {
		di.PrintTree()
	}

	dm.index = index

}

func diffPages(p1 *page, p2 *page) []string {
	d := diff.New(p1.data, p2.data)

	return d.Common()
}

// Size returns the number of pages in the index
func (dm *DiffIndex) Size() int {
	return len(dm.pages)
}

type blockRange struct {
	hash string
	data *string
}

// ProcessPage compares a page against pages within the index and returns a 
// page that is split up into blocks.
func (dm *DiffIndex) ProcessPage(url *url.URL, data []byte) *DiffMachineResult {
	dmr := newDiffMachineResult()

	sfx := suffixarray.New(data)
	hashes := dm.HashesForURL(url)

	blocks := make(map[int]*blockRange)
	positions := make([]int, 0)
	for _, hash := range hashes {

		if v, ok := dm.diffStore.Get(hash); ok {

			res := sfx.Lookup([]byte(*v), -1)

			for _, startpos := range res {
				log.Println("found position ", startpos)
				if _, ok := blocks[startpos]; !ok {
					blocks[startpos] = new(blockRange)
					blocks[startpos].hash = hash
					blocks[startpos].data = v
					positions = append(positions, startpos)
				}
			}

		} else {
			log.Println("hash not found", hash)
		}
	}

	sort.Ints(positions)

	// If there are no diffs found, create a PageBlock with containing all the data
	if len(positions) == 0 {
		pb := new(PageBlock)
		pb.Data = string(data)

		pb.Hash = ""
		dmr.PageBlocks = append(dmr.PageBlocks, pb)
		return dmr
	} else {

		datalen := len(data)

		prevpos := 0
		for _, pos := range positions {

			if prevpos < pos {
				dmr.PageBlocks = append(dmr.PageBlocks, newPageBlock("", string(data[prevpos:pos])))
			} else if prevpos > pos {
				continue
			}
			dmr.PageBlocks = append(dmr.PageBlocks, newPageBlock(blocks[pos].hash, *blocks[pos].data))
			prevpos = pos + len(*blocks[pos].data)
		}

		if prevpos < datalen {
			dmr.PageBlocks = append(dmr.PageBlocks, newPageBlock("", string(data[prevpos:datalen])))
		}

	}

	return dmr
}

func (dm *DiffIndex) HashesForURL(url *url.URL) []string {

	return dm.index.HashesForURL(url)
}

func hash(data []byte) string {
	s1 := sha1.New()
	s1.Write(data)
	return fmt.Sprintf("%x", s1.Sum(nil))
}
