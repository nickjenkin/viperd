package diffindex

import (
	"net/url"
	"testing"
)

func Test_DiffIndex_1(t *testing.T) {

	idx := New()

	u, _ := url.Parse("http://example.com/1/2/3/4/5/6")

	idx.AddPage(u, testData1())
	idx.AddPage(u, testData2())
	idx.AddPage(u, testData3())

	idx.Rebuild()

	hashes := idx.HashesForURL(u)

	expected := hash([]byte(testData1()[0:183]))
	found := false
	for _, hash := range hashes {
		if hash == expected {
			found = true
		}
	}
	if !found {
		t.Error("expected hash in result", expected)
	}

	blocks := idx.ProcessPage(u, []byte(testData1()))

	data := ""
	for _, b := range blocks.PageBlocks {
		data = data + b.Data
	}

	if data != testData1() {
		t.Error("concatinated blocks do not match original:\n", testData1(), "\n\n", data)
	}

}

func testData1() string {
	return `Phasellus luctus adipiscing ligula, non varius lorem blandit id. 
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
	Cras non velit est. Sed tristique auctor massa eu eleifend. Praesent consectetur malesuada arcu vitae dictum. 
	Proin mauris velit, bibendum ac egestas gravida, placerat ut libero. Etiam pellentesque elit ac diam pretium auctor. 
	Nam vitae mauris augue, a adipiscing felis. Sed et urna sapien. 
	Proin laoreet nulla quis dui porttitor gravida sagittis eget ante. Donec sit amet neque nibh. 
	Etiam pulvinar vestibulum sem, nec congue quam euismod quis. Aenean vel nulla risus. 
	Praesent dapibus, tortor a vulputate molestie, nisl magna lacinia erat, sed malesuada orci urna gravida dolor.`
}

func testData2() string {
	return `Duis sodales sagittis bibendum. Sed at est sapien. Phasellus adipiscing malesuada iaculis. 
	Duis egestas gravida orci, nec tristique quam lobortis ultrices. 
	Mauris ac massa sed ipsum venenatis ullamcorper. Praesent dolor enim, placerat vel porta ut, mollis euismod felis. 
	Sed placerat sodales velit id ornare. In eleifend auctor nibh, in fringilla sem hendrerit a. 
	Fusce semper, erat sit amet sagittis vehicula, libero leo hendrerit sapien, sit amet elementum sapien erat in leo. 
	Nulla quis pellentesque nisi. Nulla vestibulum mollis est et aliquet. 
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel nunc feugiat dui aliquet dapibus a a orci. 
	Sed eget ligula vel nisl lacinia semper vitae ac nibh.`

}

func testData3() string {
	return `Phasellus luctus adipiscing ligula, non varius lorem blandit id. 
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
	Cras non velit est. HALT tristique auctor massa eu eleifend. Praesent consectetur malesuada arcu vitae dictum. 
	Proin mauris velit, bibendum ac egestas gravida, placerat ut libero. Etiam pellentesque elit ac diam pretium auctor. 
	Nam vitae mauris augue, a adipiscing felis. HALT et urna sapien. 
	Proin laoreet nulla quis dui porttitor gravida sagittis eget ante. Donec sit amet neque nibh. 
	Etiam pulvinar vestibulum sem, nec congue quam euismod quis. Aenean vel nulla risus. 
	Praesent dapibus, tortor a vulputate molestie, nisl magna lacinia erat, sed malesuada orci urna gravida dolor.`
}
