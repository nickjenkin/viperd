package diffindex

import (
	"fmt"
	"strings"
)

func newDomainIndex() *domainIndex {
	di := new(domainIndex)
	//di.paths = make(map[string]map[string]*hitMiss)
	//di.hashes = make(map[string][]string)
	di.root = newPathNode("/", nil)
	return di
}

type pathNode struct {
	children map[string]*pathNode
	hashes   map[string]uint64
	parent   *pathNode
	path     string
}

func newPathNode(path string, parent *pathNode) *pathNode {
	pn := new(pathNode)
	pn.children = make(map[string]*pathNode)
	pn.hashes = make(map[string]uint64)
	pn.parent = parent
	pn.path = path
	return pn
}

func (pn *pathNode) promoteHash(hash string) {
	if _, ok := pn.hashes[hash]; ok && pn.parent != nil {
		pn.parent.hashes[hash] = pn.hashes[hash]
		delete(pn.hashes, hash)
		//	pn.parent.promoteHash(hash)
	}
}

func (pn *pathNode) insertHash(hash string) {

	if pn.parent != nil {
		sibs := pn.parent.decendentsWithHash(hash)

		if len(sibs) > 0 {
			//pn.parent.insertHash(hash)
			for _, s := range sibs {
				//s.print(0)
				s.deleteHash(hash)
			}
			pn.parent.insertHash(hash)
		} else {
			pn.hashes[hash] = 1
		}
	} else {
		pn.hashes[hash] = 1
	}

}

func (pn *pathNode) decendentsWithHash(hash string) []*pathNode {
	result := make([]*pathNode, 0)

	for _, c := range pn.children {
		if _, ok := c.hashes[hash]; ok {
			result = append(result, c)
		} else {
			for _, r := range c.decendentsWithHash(hash) {
				result = append(result, r)
			}
		}
	}

	return result

}

func (pn *pathNode) print(level int) {
	fmt.Printf("%s%s -> %s\n", strings.Repeat(" ", level), pn.path, pn.hashes)
	level++
	for _, c := range pn.children {
		c.print(level)
	}
}

func (pn *pathNode) deleteHash(hash string) {
	delete(pn.hashes, hash)
}

type domainIndex struct {
	// path -> hash index + hit count
	paths map[string]map[string]uint64
	// hash -> path index + hit count
	hashes map[string]map[string]*pathNode

	root *pathNode
}

func (di *domainIndex) AddPath(path, hash string) {

	pn := di.getPathNodeForPath(path, true)

	pn.insertHash(hash)

	//log.Println(path, hash, di.paths[path][hash])
}

func (di *domainIndex) PrintTree() {
	di.root.print(0)
}

func (di *domainIndex) getPathNodeForPath(path string, create bool) *pathNode {
	if path[len(path)-1:] == "/" {
		path = path[0 : len(path)-1]
	}
	p1b := strings.Split(path, "/")

	if len(p1b) > 1 {
		start := di.root
		prefix := p1b[0]
		for i := 1; i < len(p1b); i++ {
			prefix = prefix + "/" + p1b[i]
			if pn, ok := start.children[prefix]; ok {
				start = pn
			} else {
				if create {
					start.children[prefix] = newPathNode(prefix, start)
					start = start.children[prefix]
				} else {
					return start
				}
			}
		}
		return start
	}
	return di.root
}

func (di *domainIndex) HashesForPath(path string) {

}

func (di *domainIndex) TrimIndex() {
}
