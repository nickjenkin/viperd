/**
 * Copyright 2013 Nick Jenkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package diffindex

import (
	"log"
	"net/url"
	"strings"
)

type index struct {
	domains map[string]*domainIndex
	diffIdx map[string]*pageDiff
	total   int64
}
type hitMiss struct {
	hit  int64
	miss int64
}

func newIndex() *index {
	idx := new(index)
	idx.domains = make(map[string]*domainIndex)
	idx.diffIdx = make(map[string]*pageDiff)
	return idx
}

func (idx *index) HashesForURL(url *url.URL) []string {

	paths := pathsForURL(url)
	//log.Println(paths)
	diffs := make([]string, 0)
	dom := idx.getDomain(url)

	for _, path := range paths {
		//log.Println("checking path", path)
		if hashes, ok := dom.paths[path]; ok {
			//	log.Println(hashes)
			for hash, _ := range hashes {
				diffs = append(diffs, hash)
			}
		}
	}

	return diffs
}

func (idx *index) AddDiff(url *url.URL, hash string) {
	if _, ok := idx.diffIdx[hash]; !ok {
		idx.diffIdx[hash] = newPageDiff(hash)
	}

	dom := idx.getDomain(url)

	log.Println(url.Path, hash)
	dom.AddPath(url.Path, hash)
	/*
		paths := pathsForURL(url)
		//pd := idx.diffIdx[hash]

		for _, path := range paths {
			log.Println(path, hash)
			dom.AddPath(path, hash)
		}*/

	idx.total++

}

func (idx *index) getDomain(url *url.URL) *domainIndex {
	log.Println("host", url.Host)
	if _, ok := idx.domains[url.Host]; !ok {
		idx.domains[url.Host] = newDomainIndex()
	}

	return idx.domains[url.Host]
}

func pathsForURL(url *url.URL) []string {
	res := make([]string, 0)

	subpaths := strings.Split(url.Path, "/")
	suffix := ""

	for _, sp := range subpaths {
		if sp != "" {
			suffix = suffix + "/" + sp
			res = append(res, suffix)
		}
	}
	return res
}
