![viperd](http://nickjenkin.com/viperd/viperdx.png)
======

Viperd is a reverse proxy that finds common HTML elements (headers, footers etc.) and caches them in the web browsers local storage.

Usage
-----

Viperd is written in Go. You'll need to [install](http://golang.org/doc/install) Go and setup your Go environment.

```bash
go get github.com/nickjenkin/viperd
go install github.com/nickjenkin/viperd
viperd -bind 0.0.0.0:8080 -reverse 127.0.0.1:80
```

To recompile assets, you will also need [go-bindata](http://github.com/jteeuwen/go-bindata):
```bash
go get github.com/jteeuwen/go-bindata
go install github.com/jteeuwen/go-bindata
./build.sh
```

How does it work?
-----------------
Viperd keeps track of pages that have been visited (not nessesarily all of them), and does a diff to find common elements between pages X and Y. The common elements are added to an index by splitting the URL of the page up into segments (split by "/"). 

So the index might intially look like:

> example.com -> []  
> example.com/blog/ -> [abc, def, sdf]  
> example.com/blog/2013/ -> [abc, def, sdf]  
> example.com/blog/2013/viperd-is-not-for-production -> [abc, def, sdf]  
> example.com/blog/2013/penguins-are-delicious -> [abc, def, sdf]

The index is then trimmed, moving common elements back to to their most common parent:

> example.com -> []  
> example.com/blog/ -> [abc, def, sdf]

When a request comes in for "example.com/blog/2012/viperd-is-not-for-production", the common elements [abc, def, sdf] are not sent to the browser. (provided they actually exist in the HTML of the requested page)

As for the browser side of things, the page is split up into a JSON encoded array. Either a hash or actual content will be sent in the array. If the block is a hash, the browser checks it's localStorage (HTML5 feature), and loads it from there. If the browser does not have a local cache of the hash block, it is requested and then stored.

Viperd only optimizes pages with the HTML content type, and are not an XHR request. If the browser does not have localStorage support Viperd is disabled for future requests (the page will still load correctly initially however).

Known Issues
------------

* JSON encoding of HTML requires unicode escaping, thus creating a large amount of additional characters. 
* Viperd makes assumptions that URLs follow some reasonable hierarchy. While optimizations will continue to function if there is none,
the performance of the indexing will degrade.
* Viperd does not have HTTPS support (and probably never will).

Is Viperd secure?
-----------------
Vulnerable to hash leaking (in the event a hash contained personal content).

Should I use Viperd for my website?
-----------------------------------
No

When should I use Viperd?
-------------------------
After significant amounts of beer. Viperd is an implementation of an interesting idea. Performance gains are minimal and using viperd increases the overall complexity of a website.

